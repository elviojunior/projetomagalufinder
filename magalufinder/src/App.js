import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

addTodo(event) {

  event.preventDefault();
  let usuario = this.refs.usuario.value;
  let senha = this.refs.senha.value;

  let todo = { 
    usuario,
    senha
  };

  let todos = this.state.todos;

  todos.push(todo);

  this.setState({
    todos: todo
  })


}
constructor() {

  super();
  this.addTodo = this.addTodo.bind(this);
  this.state = {
    todos: [],
    title: 'MagaluFinder App'
  }
}

  render() {

      let title = this.state.title;
      let todos = this.state.todos;

    return (

      <div className="App">
        <h1>{title}</h1>
        <form>
         <input type="text" ref="usuario" placeholder="digite o usuario" />
         <input type="text" ref="senha" placeholder="digite a senha" />
         <button onClick={this.addTodo}>Add todo </button>
        </form>
        <pre>
          {JSON.stringify(todos)}
        </pre>
      </div>
    );
  }
}

export default App;
