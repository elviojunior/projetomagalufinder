(function () {
  'use strict';

  function openModal() {
    var modalTrigger = document.getElementsByClassName('js-modal');

    for (var i=0; i < modalTrigger.length; i++) {
      modalTrigger[i].onclick = function(event) {
        event.preventDefault();

        var target = this.getAttribute('href').substr(1);
        var modalWindow = document.getElementById(target);

        modalWindow.classList ? modalWindow.classList.add('is-open') : modalWindow.className += ' is-open';
      };
    }
  }

  function closeModal() {
    var modalTrigger = document.getElementsByClassName('js-modal-close');

    for (var i=0; i < modalTrigger.length; i++) {
      modalTrigger[i].onclick = function(event) {
        event.preventDefault();

        // Get element tree parentNode for js-modal-close
        var modalWindow = this.parentNode.parentNode.parentNode;

        closeModal(modalWindow);

        // add event click to overlay
        modalWindow.onclick = function (event) {
          event.preventDefault();
          // closeModal(this);
        };
      };
    }

    var closeModal = function (modalWindow) {
      modalWindow.classList ? modalWindow.classList.remove('is-open') : modalWindow.className.replace('is-open', '');
    };
  }

  function ready (fn) {
    if (document.readyState != 'loading') {
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }

  ready(openModal);
  ready(closeModal);
}());